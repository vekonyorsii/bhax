#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <iomanip>

int main (void)
{
	WINDOW *doboz; 		// window objektum (curses.h-ban), nevezzük el "doboz"-nak
	doboz = initscr ();	// a doboz értékének adjuk meg az ablak jelenlegi méretét
	int szelesseg = 0;
	int magassag = 0;	//bal felül kezdünk (0,0) pontból
	int szelsolepes = 1;
	int magassaglepes = 1;	//a kezdési ponttól jobb le tudunk indulni, ezért mindkét érték pozitív
	int maxszelesseg;
	int maxmagassag;

	while (true) 		//végtelen ciklus
	{
		getmaxyx (doboz, maxmagassag, maxszelesseg); 	//a függvény lekéri a dobozunk (window) aktuális méreteit, ezt két változóban tárolja el
		mvprintw (magassag, szelesseg, "O"); 		// a függvény a dobozunk megadott koordinátájába ír egy "O" karaktert
		refresh ();
		usleep (50000);	//késleltet, minél kisebb az érték, annál gyorsabban fog mozogni a labda
		clear ();	// nem fogja az előző lépést mutatni
		szelesseg += szelsolepes; //jobbra vagy balra lépés
		magassag += magassaglepes;	//le vagy fel lépés

		if (szelesseg >= maxszelesseg - 1)		// amint a labda jobb szélre ér
			szelsolepes = -1;
		if (szelesseg <= 0)				// bal oldalt érinti a labda
			szelsolepes = 1;
		if (magassag >= maxmagassag -1 )		// a labda eléri a doboz alját
			magassaglepes = -1;
		if (magassag <= 0)				// amint a doboz tetejét éri el a labda
			magassaglepes = 1;
	}
return 0;
}
