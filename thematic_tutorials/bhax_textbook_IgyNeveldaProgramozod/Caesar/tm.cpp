#include <iostream>

int main ()
{
    int nr = 5;
    double **tm;
    
    std::cout << &tm << std::endl;
    
    tm = new double*[5];

    std::cout << tm << std::endl;
    
    for (int i = 0; i < nr; ++i)
    {
        tm[i] = new double[i + 1];

    }

    std::cout << tm[0] << std::endl;    
    
    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    tm[3][0] = 42.0;
    (*(tm + 3))[1] = 43.0;
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    for (int i = 0; i < nr; ++i)
        delete[] tm[i];

    delete[] tm;

    return 0;
}
