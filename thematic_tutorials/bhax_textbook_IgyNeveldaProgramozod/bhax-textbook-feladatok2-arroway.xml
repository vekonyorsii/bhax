<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Arroway!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>OO szemlélet</title>
        <para>A módosított polártranszformációs normális generátor beprogramozása Java nyelven. Mutassunk rá,
            hogy a mi természetes saját megoldásunk (az algoritmus egyszerre két normálist állít elő, kell egy
            példánytag, amely a nem visszaadottat tárolja és egy logikai tag, hogy van-e tárolt vagy futtatni kell
            az algot.) és az OpenJDK, Oracle JDK-ban a Sun által adott OO szervezés ua.!</para>
        <para>Lásd még fóliák!</para>
        <para>Ismétlés: https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog1_5.pdf (16-22 fólia)
            Ugyanezt írjuk meg C++ nyelven is! (lásd még UDPROG repó: source/labor/polargen)</para>
        <para>
            Megoldás forrása: <link xlink:href="https://github.com/tothferenc98/prog2/blob/main/Arroway/PolarGenerator.java">PolarGenerator.java</link> és <link xlink:href="https://github.com/tothferenc98/prog2/blob/main/Arroway/polar.cpp">polar.cpp</link>
        </para>
        <para>
            A  <emphasis role='bold'>
                    módosított polártranszformációs normális generátor
                </emphasis>
            egy olyan algoritmus, amivel mi kis hardverigénnyel tudunk számokat generálni.
            Az algoritmus ezen előnyét az optimalizáltsága adja, amely egyszerre két véletlen számot generál, az egyiket akkor adja vissza, a másikat pedig letárolja és ha újra meghívják akkor nem kell legenerálnia, hanem csak a tárolt randon számot adjuk vissza, így időt takarítva.
        </para>
        
        <para>
            Az implementálását ebben a feladatban elvégeztük Java és C++ programozási nyelveken is.
        </para>
        <programlisting language='Java'>
            <![CDATA[
public class PolarGenerator
{
    boolean notStored = true;
    double storedValue;
    
    public PolarGenerator()
    {
        notStored = true;
    }
    
    public double next()
    {
        
        if(notStored)
        {
            double u1,u2,v1,v2,w;
            do
            {
            
                u1 = Math.random();
                u2 = Math.random();
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 -1;
                w = v1*v1 + v2*v2;
            
            } while(w>1);
            
            double r = Math.sqrt((-2*Math.log(w))/w);
            storedValue = r* v2;
            notStored = !notStored;
            return r * v1;
        }
        else
        {
            notStored = !notStored;
            return storedValue;
        }
    }
    ]]></programlisting>
    <para>
        Először javaban nézzük meg.
    </para>
    <para>
        Kezdetben létrehozunk egy boolean változót, amely tárolja, hogy van-e letárolva második random szám. A double fogja tárolni konkrétan ezt az értéket.
    </para>
    <para>
        A next függvénnyel fogjuk folytatni, itt először megvizsgáljuk hogy létezik-e a második random számunk. Amennyiben nincs, akkor létrehozzuk azt. A random szám generálásához szükségünk van a Math osztályra.
        Ha eredetileg is létezett a random számunk, akkor pedig visszaadjuk a mentett értéket és false-ra állítjuk az második random szám tárolóját.
    </para>
        
        <para>
            Most pedig nézzük a C++ változatot:
        </para>
        <programlisting language='C++'>
            <![CDATA[
#include <iostream>
#include <cstdlib>  
#include <cmath>    
#include <ctime>    

class PolarGen 
{

    public:

        PolarGen(); 
        double next(); 
        ~PolarGen(){} 

    private:
        bool notStored;
        double storedValue;
};


PolarGen::PolarGen()
{
    notStored = true;
    std::srand (std::time(NULL));
};


double PolarGen::next() 
{ 
    if (notStored)
{
        double u1, u2, v1, v2, w;

        do
        {
            u1 = std::rand () / (RAND_MAX + 1.0); 
            u2 = std::rand () / (RAND_MAX + 1.0);
            v1 = 2 * u1 - 1;
            v2 = 2 * u2 - 1;
            w = v1 * v1 + v2 * v2;
        }
        while (w > 1);

        double r = std::sqrt ((-2 * std::log (w)) / w);

        storedValue = r * v2; 
        notStored = !notStored;

        return r * v1; 
    }
    
    else
    {
        notStored = !notStored;
        return storedValue;
    }
};    

int main() 
{

    PolarGen rnd;

    for (int i = 0; i < 10; ++i) 
        std::cout << rnd.next() << std::endl;

}
            
]]>
        </programlisting>
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/random_teszt.png" scale="60"></imagedata>
            </imageobject>
            <caption>
                <para>
                    Java és C++ változat futtatása
                </para>
            </caption>
        </mediaobject>
        <para>
            A Java Sun csapata már a kezdetekben így gondolkozott, ezt bizonyítja, hogy a JDK-ba is integrálták ezt az algoritmust. 
        </para>
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/random_java_sun.png" scale="60"></imagedata>
            </imageobject>
            <caption>
            <para>
                Sun-os megoldás
            </para>
            </caption>
        </mediaobject>
        
    </section> 

    <section>
        <title>„Gagyi”</title>
        <para>Az ismert formális „while <![CDATA[(x <= t && x >= t && t != x);”]]> tesztkérdéstípusra adj a szokásosnál
            (miszerint x, t az egyik esetben az objektum által hordozott érték, a másikban meg az objektum
            referenciája) „mélyebb” választ, írj Java példaprogramot mely egyszer végtelen ciklus, más x, t
            értékekkel meg nem! A példát építsd a JDK Integer.java forrására, hogy a 128-nál inkluzív objektum
            példányokat poolozza!</para>
        <para>
            Ez a feladat a Java alap működésére kíváncsi, a fő kérdés az, hogy mikor egyenlő két objektum.
        </para>
        <para>
            Két egyenlőséget tudunk megkülönböztetni az egyik az érték szerinti a másik az identitáson alapuló egyenlőség,
        </para>
        <para>
            Az érték szerinti egyenlőség egyértelműen az értékeket vizsgálja, ha azok egyenlőek, akkor megegyezik az értékük.
        </para>
        <para>
            Példa:
        </para>
        <para>
        <programlisting language="java"><![CDATA[
        import java.lang.Number;

        class Gagyi
        {
            
            
            public static void main(String[] args)
            {
                Integer t = 750;
                Integer x = 750;
                
                while(x <= t && x>=t && t != x)
                    System.out.println("Kiír");
                
                 
            }


        }
        ]]></programlisting>
            A képen látható, hogy végtelen ciklust eredményezett. Ez azért történt így, mivel teljesül a feltétel összes eleme. Az első <![CDATA[x <= t -nél egyértelmű, mivel 750<=750]]>, a másodiknál is ez a helyzet, a harmadik pedig elgondolkodtató. <![CDATA[Itt t != x]]> azért teljesül, mivel az osztályból két példány van példányosítva, és ez lehetetlen, mivel akkor egy helyen kellene lenniük a memóriában.
        </para>
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/IntegerEgyenlo.png" scale="40"></imagedata>
            </imageobject>
            
        </mediaobject>
        <para>
            Viszont meg tudjuk szüntetni a while ciklust úgy, hogy csak az Integerek értékét lecsökkentjük, ez annak köszönhető, hogy az Integer osztály rendelkezik gyorsítótárazással.
            <programlisting language="java"><![CDATA[
        import java.lang.Number;

        class Gagyi
        {
            
            
            public static void main(String[] args)
            {
                Integer t = 75;
                Integer x = 75;
            
                while(x <= t && x>=t && t != x)
                    System.out.println("Kiír");

            }

        }            

        ]]></programlisting>
        </para>
        <mediaobject>
            <imageobject>
                <imagedata fileref="img/IntegerEgyenlo2.png" scale="40"></imagedata>
            </imageobject>
        </mediaobject>
        <para>
            Az Integer osztály gyorsítótárazása [-128;127] tartományban tárol, vagyis az ellenrízni kívánt értéknek ezen tartományban kell lennie, hogy ne teljesüljön a while. Amikor mi ebből a tartományból választunk értéket, olyankor mi már egy létező objektumra kapunk referenciát, így nem teljesült az x != t feltétel
        </para>
        <para>
            Minderre azért volt szükség, hogy megtanuljuk, ha két objektumot akarunk összehasonlítani, inkább használjuk az equals függvényt, úgy nem tudjuk elrontani.
        </para>
    </section>

    <section>
        <title>EPAM: Java Object metódusok</title>
        <para>Mutasd be a Java Object metódusait és mutass rá mely metódusokat érdemes egy saját
            osztályunkban felüldefiniálni és miért. (Lásd még Object class forráskódja)
</para>
    <para>
        A Java-ban minden osztály közvetetten vagy közvetlenül az Object osztályból ered.
	</para>
    <para>
        Az Object class metódusai:
    </para>
 
        <itemizedlist>
            <listitem>
                <para>
                    toString(): azért használjuk, hogy egy string reprezentációt nyerjünk egy objektumból. Ha nem definiáljuk a toString() metódust az osztályban, akkor az Object class toString() metódusa kerül meghívásra.
                </para>
            </listitem>
            <listitem>
                <para>
                    hashCode(): egy string hash kódjával tér vissza. A hash-kód értékét hash-alapú gyűjteményekben használják, például HashMap, HashTable stb. Abban az esetben, ha felüldefiniáltuk a <literal>equals()</literal>-t, ezt is kell, hogy a kettő összhangban legyen.
                </para>
            </listitem>
            <listitem>
                <para>
                    equals(): Visszatérési értéke <literal>true</literal> vagy <literal>false</literal>. Azt ellenőrzi, hogy két érték egyenlő-e.
                </para>
            </listitem>
            <listitem>
                <para>
                    getClass(): visszatérési értéke egy objektum runtime osztálya.
		</para>
            </listitem>
            <listitem>
                <para>
                    finalize(): felülbírálja a rendszer erőforrásait, megtisztítási tevékenységeket végez és minimalizálja a memória szivárgását. Ezt általában közvetlenül az objektum szemétgyűjtése előtt hívják meg.
                </para>
            </listitem>
            <listitem>
                <para>
                    clone(): másolás. Létrehoz az aktuális osztályból egy új példányt. Lényegben egy az egyben leklónoz egy objektumot.
                </para>
            </listitem>
            <listitem>
                <para>
                    notify(): egy szál felébresztésére használják. Egy szálról kap értesítést, amely egy adott objektumra vár. Ha több szál várja az értesítést, akkor is csak egy szál kapja meg, a többi további értesítésre vár.
                </para>
            </listitem>
            <listitem>
                <para>
                    notifyAll(): az összes szál felébresztésére használják. Az összes várakozó szál kap értesítést az objektről.
                </para>
            </listitem>
            <listitem>
                <para>
                    wait(): egy szál addig vár, míg egy másik meg nem hívja a <literal>notify()</literal> vagy a <literal>notifyAll()</literal> metódust.
		</para>
            </listitem>
            <listitem>
                <para>
                    wait(long timeout): hasonló mint az előző: egy szál addig vár, míg egy másik meg nem hívja a <literal>notify()</literal> vagy a <literal>notifyAll()</literal> metódust, vagy amíg a megadott idp le nem telik.
                </para>
            </listitem>
            <listitem>
                <para>
                    wait(long timeout int nanos): egy szál addig vár, míg egy másik meg nem hívja a <literal>notify()</literal> vagy a <literal>notifyAll()</literal> metódust, vagy amíg a megadott idp le nem telik, vagy egy másik szál meg nem zavarja.
                </para>
            </listitem>
        </itemizedlist>
    </section> 


    <section>
        <title>EPAM: Eljárásorientál vs Objektumorientált</title>
        <para>Írj egy 1 oldalas értekező esszé szöveget, amiben összehasonlítod az eljárásorientált és az
            objektumorientált paradigmát, igyekezve kiemelni az objektumorientált paradigma előnyeit!</para>
        <!--<para>Object-oriented Programming Languages

            Object-oriented Programming is a programming language that uses classes and objects to create models based on the real world environment. An Object-oriented Programming application may use a collection of objects which will pass messages when called upon to request a specific service or information. Objects are able to pass, receive messages or process information in the form of data.

            One reason to use Object-oriented Programming is because it makes it easy to maintain and modify existing code as new objects are created inheriting characteristics from existing ones (SEH, 2013). This cuts down the development time considerably and makes adjusting the program much simpler.

            Another reason to use Object-oriented Programming is the ease of development and ability for other developers to understand the program after development. Well commented objects and classes can tell a developer the process that the developer of the program was trying to follow. It can also make additions to the program much easier for the new developer.

            The last reason to use Object-oriented Programming that I will mention here is the efficiency of the language. Many programming languages using Object-oriented Programming will dump or destroy unused objects or classes freeing up system memory. By doing this the system can run the program faster and more effectively.</para>
            <para>Procedural Programming Languages

            Procedural Programming, which at times has been referred to as inline programming, takes a more top-down approach to programming. Object-oriented Programming uses classes and objects, Procedural Programming takes on applications by solving problems from the top of the code down to the bottom.

            This happens when a program starts with a problem and then breaks that problem down into smaller sub-problems or sub-procedures. These sub-procedures are continually broken down in the process called functional decomposition until the sub-procedure is simple enough to be solved.

            The issue that is obvious in Procedural Programming is that if an edit is needed to the program, the developer must edit every line of code that corresponds to the original change in the code. An example would be if at the beginning of a program a variable was set to equal the value of 1. If other sub-procedures of the program rely on that variable equaling 1 to function properly they will also need to be edited. As more and more changes may be needed to the code, it becomes increasingly difficult to locate and edit all related elements in the program.</para>
            <para>Simple comparison between Object-oriented Programming and Procedural Programming

            A simple way to compare both programming methods is to think of Object-oriented Programming as  learn to read picture book. As children see pictures of simple objects like a house or picture they know that throughout the book when they see a picture of the house it represents the word house. The children can then read through the book as words are substituted throughout the story with pictures.

            In Object-oriented Programming the classes could represent the pictures in the learn to read books. A house could represent a class and anything the developer wants to have included to describe that house like the color, size, number of bathrooms etc.

            In Procedural Programming the learn to read book would be words on the page without pictures to help guide the young learner through the book. If the story was changed in the beginning of the book it could disrupt or make the story later on in the book not make any sense. Although learning to read the book would make programming with Procedural Programming simple, it would make it difficult for other readers in the case of the book or programmers in the case of Procedural Programming to add to the story.

            Procedural Programming also uses different methods throughout the code than Object-oriented Programming.

            As an example Object-oriented Programming uses methods where Procedural Programming uses procedures.

            Object-oriented Programming uses objects where Procedural Programming uses records.

            Object-oriented Programming uses classes where Procedural Programming uses modules and Object-oriented Programming uses messages where Procedural Programming uses procedure calls.</para>-->
        <para>Objektumorientált programozási nyelvek

            Az objektumorientált programozás egy olyan programozási nyelv, amely főként osztályokat és objektumokat használ a valós világon alapuló modellek létrehozásához. Viszont vannak köztük olyanok is, amelyek nem ismerik az osztály fogalmát. Ilyen például a JavaScript. Az objektumorientált programozó alkalmazás olyan objektumok gyűjteményét használhatja, amelyek üzeneteket továbbítanak, ha egy adott szolgáltatás vagy információ kérésére hívják őket. Az objektumok képesek üzeneteket továbbítani, fogadni vagy adatokat feldolgozni.

            Az objektum-orientált programozás egyik oka az, hogy megkönnyíti a meglévő kód karbantartását és módosítását, mivel új objektumok jönnek létre, amelyek a meglévő jellemzőit öröklik (SEH, 2013). Ez jelentősen lerövidíti a fejlesztési időt, és sokkal egyszerűbbé teszi a program kiigazítását.

            Az objektum-orientált programozás másik oka az egyszerű fejlesztés és a képesség, hogy a fejlesztők megértsék a programot a fejlesztés után. A jól kommentált objektumok és osztályok elmondhatják a fejlesztőnek azt a folyamatot, amelyet a program fejlesztője megpróbált követni. Sokkal könnyebbé teheti a program kiegészítéseit az új fejlesztő számára.

            Az objektum-orientált programozás utolsó oka, amelyet itt megemlítek, a nyelv hatékonysága. Az objektumorientált programozást használó sok programozási nyelv kiüríti vagy megsemmisíti a fel nem használt objektumokat vagy osztályokat, amelyek felszabadítják a rendszer memóriáját. Ezzel a rendszer gyorsabban és hatékonyabban tudja futtatni a programot.
</para>
        <para>Eljárási programozási nyelvek

            Az eljárási programozás, amelyet időnként inline programozásnak neveznek, felülről lefelé irányítja a programozást. Az objektumorientált programozás osztályokat és objektumokat használ, az eljárási programozás úgy veszi fel az alkalmazásokat, hogy megoldja a problémákat a kód tetejétől lefelé.

            Ez akkor történik, amikor egy program problémával indul, majd ezt a problémát kisebb részproblémákra vagy alfolyamatokra bontja. Ezeket az alfolyamatokat folyamatosan bontják a funkcionális bontásnak nevezett folyamatban, amíg az alfolyamat nem elég egyszerű ahhoz, hogy megoldható legyen.

            Az eljárási programozásban nyilvánvaló az a kérdés, hogy ha szerkesztésre van szükség a programban, akkor a fejlesztőnek minden olyan kódsort szerkesztenie kell, amely megfelel a kód eredeti változásának. Példaként említhetjük, ha a program elején egy változót 1-es értékre állítottunk be. Ha a program más alprogramjai arra hivatkoznak, hogy a változó megfelel az 1-nek, akkor azokat szintén szerkeszteni kell. Mivel egyre több változtatásra lehet szükség a kódban, egyre nehezebb megtalálni és szerkeszteni a program összes kapcsolódó elemét.</para>
        <para>Egyszerű összehasonlítás az objektum-orientált programozás és az eljárási programozás között

            A programozási módszerek összehasonlításának egyszerű módja az, ha az objektum-orientált programozást úgy gondoljuk, mint megtanulni olvasni a képeskönyvet. Amikor a gyerekek egyszerű tárgyakról, például házról vagy képről látnak képet, tudják, hogy az egész könyvben, amikor a ház képét látják, ez a ház szót jelenti. Ezután a gyerekek végigolvashatják a könyvet, amikor a szavak a történetben képekkel helyettesülnek.

            Az objektumorientált programozásban az osztályok képviselhetik a képeket a tanulni tanulni könyvekben. Egy ház képviselhet egy osztályt és bármit, amit a fejlesztő bele akar foglalni a ház leírására, mint például a színe, mérete, a fürdőszobák száma stb.

            Az eljárási programozásban a könyv megtanulása szavak lennének az oldalon képek nélkül, amelyek segítik a fiatal tanulót a könyvben. Ha a történetet megváltoztatták a könyv elején, az megzavarhatja, vagy a könyv későbbi részében nincs értelme. Bár a könyv olvasásának megtanulása egyszerűvé tenné a programozást az eljárási programozással, megnehezítené más olvasók számára a könyv esetében, vagy a programozók számára az eljárási programozás esetén a történethez való hozzáadást.

            Az eljárási programozás más módszereket is használ a kódban, mint az objektum-orientált programozás.

            Példaként az objektum-orientált programozás olyan módszereket használ, ahol az eljárási programozás eljárásokat használ.

            Az objektum-orientált programozás olyan objektumokat használ, ahol az eljárási programozás rekordokat használ.

            Az objektumorientált programozás olyan osztályokat használ, ahol az eljárási programozás modulokat, az objektum-orientált programozás pedig üzeneteket használ, ahol az eljárási programozás eljáráshívásokat használ.</para>

    </section>
  
<section>
        <title>Yoda</title>
        <para>
            Feladatunk egy olyan program előállítása volt, amely NullPointerException-nel áll le futás közben, a Yoda feltételek használata nélkül, ha viszont alkalmazzuk ezeket, akkor lefut.
        </para>
        <para>
           Mik a Yoda feltételek?  A feltételes kifejezések tipikus formájától eltérő, fordított megadása.
        </para>
        <para>
            Szerepe akkor jelentős, ha egy konstans értéket hasonlítunk össze egy változó tartalmával, ugyanis itt megeshet, hogy a változóban esetleg semmilyen érték nincsen, ez pedig hibákhoz vezethet.
        </para>
        <para>
            A mi példánkban egy NULL értékű String változót próbálunk egy akármilyen szöveghez hasonlítani. Mint ahogy sejteni is lehetett, ez kicsit problémás lesz. A semmit hogyan is tudnánk hasnolítani? Ez bármelyik más példányszintű metódus hívása esetén is hibába futna.
        </para>
        <programlisting><![CDATA[
public class Yoda{
    public static void main(String args[]){
        
        String szoveg = null;
        
        //>:(
        
        try{
            if(szoveg.equals("asd123")){}
        }
        catch(Exception e){System.out.println(">:( ----->> "+e);}
        
        
    }
}
                ]]></programlisting> 
        <para>
            Futtatás közben <literal>NullPointerException-t</literal> dobva leállna a programunk, ha nem kapnánk el egy try-catch-el.
        </para>
        
        <para>
            Erre a problémára jó a Yoda Conditions. Ez lényegében megfordítja a vizsgálatot és így biztosítja, hogy ne a semmihez hasonlítsunk. 
        </para>
        <programlisting><![CDATA[
public class Yoda{
    public static void main(String args[]){
        
        String szoveg = null;
        
        //:)
        if("asd123".equals(szoveg)){
            System.out.println(":)");
        }
        
    }
}
                ]]></programlisting> 
        
        <para>
            Tehát nem kapunk errort (akármi is van a szoveg változóban), a vizsgálat eredménye <literal>false</literal>, és a program hibátlanul lefut.
        </para>
</section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
</chapter>                
